Analysis on stock exchange data to find the lowest **low** and occurrance **date** for every **file** located in **/Data-Sets/stock-exchange/amex-nyse-nasdaq-stock-histories/full_history** directory of the dataset.

**Define CSV file path in `Main.java` before run this program.**