package me.abdurrahim;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class CsvParser {
    private File csvFile = null;
    private String filePath;
    private ArrayList<String[]> rows = new ArrayList<String[]>();

    public CsvParser(File csvFile) throws Exception {
        if (csvFile == null || !csvFile.exists()) {
            throw new Exception("Invalid file");
        }

        this.csvFile = csvFile;
        this.filePath = csvFile.getAbsolutePath();
    }

    public CsvParser(String filePath) throws Exception {
        this.filePath = filePath;

        if (filePath.equals("") || filePath == null) {
            throw new Exception("Invalid filename");
        }

        csvFile = new File(filePath);

        if (!csvFile.exists() || !csvFile.isFile()) {
            throw new Exception("File doesn't exists");
        }
    }

    public void parse() throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            try {
                String[] cells = line.split(",");
                rows.add(cells);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    public ArrayList<String[]> getRows() {
        return rows;
    }
}