package me.abdurrahim;

import java.io.File;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.start();

        // CSV file path
        File direcory = new File("");

        HashMap<String, StockExchangeModel> hashmap = new HashMap<String, StockExchangeModel>();

        for (File csvFile : direcory.listFiles()) {
            String filename = csvFile.getName();
            try {
                CsvParser parser = new CsvParser(csvFile);
                parser.parse();
                for (String[] row : parser.getRows()) {
                    String date = row[0];
                    String value = row[row.length - 2];

                    try {
                        double newDouble = Double.parseDouble(value);

                        if (hashmap.containsKey(filename)) {
                            StockExchangeModel existingData = hashmap.get(filename);

                            if (existingData.getLow() > newDouble) {
                                existingData.setDate(date);
                                existingData.setLow(newDouble);
                            }
                        } else {
                            hashmap.put(filename, new StockExchangeModel(date, newDouble));
                        }
                    } catch (Exception ex) {
                        // Do nothing
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        timer.stop();
        System.out.println(timer.showElapedTime());
        System.out.println(timer.showStartTime());
        System.out.println(timer.showEndingTime());

        hashmap.forEach((s, model) -> System.out.println(s + "\t" + model.getLow() + "\t" + model.getDate()));
    }
}
