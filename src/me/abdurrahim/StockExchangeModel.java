package me.abdurrahim;

public class StockExchangeModel {
    private String date;
    private double low;

    public StockExchangeModel(String date, double low) {
        this.date = date;
        this.low = low;
    }

    public double getLow() {
        return low;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setLow(double high) {
        this.low = high;
    }
}