package me.abdurrahim;

import java.util.HashMap;

public class Task {
    private HashMap<String, Double> hashmap = new HashMap<String, Double>();
    private static Task task;

    private Task(){

    }

    public static Task Initialize (){
        if(task == null){
            task = new Task();
        }

        return task;
    }

    public HashMap<String, Double> getHashmap() {
        return hashmap;
    }

    public void Manage(String[] row){
        String date = row[1];
        String value = row[row.length - 2];

        try {
            double newDouble = Double.parseDouble(value);

            if (hashmap.containsKey(date)) {
                Double existingValue = hashmap.get(date);

                if (existingValue > newDouble) {
                    hashmap.put(date, newDouble);
                }
            } else {
                hashmap.put(date, newDouble);
            }
        } catch (Exception ex) {
            // Do nothing
        }
    }
}
