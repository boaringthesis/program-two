package me.abdurrahim;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Timer {
    private Date startTime;
    private Date endTime;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    public void start() {
        startTime = new Date();
    }

    public void stop() {
        endTime = new Date();
    }

    public String showStartTime() {
        return dateFormat.format(startTime);
    }

    public String showEndingTime() {
        return dateFormat.format(endTime);
    }

    public String showElapedTime() {
        long diff = endTime.getTime() - startTime.getTime();
        return String.valueOf(diff / 1000) + " seconds";
    }
}